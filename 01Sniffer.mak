# Microsoft Developer Studio Generated NMAKE File, Based on 01Sniffer.dsp
!IF $(CFG)" == "
CFG=01Sniffer - Win32 Debug
!MESSAGE No configuration specified. Defaulting to 01Sniffer - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "01Sniffer - Win32 Release" && "$(CFG)" != "01Sniffer - Win32 Debug"
!MESSAGE 指定的配置 "$(CFG)" 无效.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "01Sniffer.mak" CFG="01Sniffer - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "01Sniffer - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "01Sniffer - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF $(OS)" == "Windows_NT
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "01Sniffer - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# 开始自定义宏
OutDir=.\Release
# 结束自定义宏

ALL : "$(OUTDIR)\01Sniffer.exe" "$(OUTDIR)\01Sniffer.bsc"


CLEAN :
	-@erase "$(INTDIR)\EncDec.obj"
	-@erase "$(INTDIR)\EncDec.sbr"
	-@erase "$(INTDIR)\MD5.obj"
	-@erase "$(INTDIR)\MD5.sbr"
	-@erase "$(INTDIR)\01Sniffer.obj"
	-@erase "$(INTDIR)\01Sniffer.pch"
	-@erase "$(INTDIR)\01Sniffer.res"
	-@erase "$(INTDIR)\01Sniffer.sbr"
	-@erase "$(INTDIR)\01SnifferDlg.obj"
	-@erase "$(INTDIR)\01SnifferDlg.sbr"
	-@erase "$(INTDIR)\SHA1.obj"
	-@erase "$(INTDIR)\SHA1.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\ToolTipDlg.obj"
	-@erase "$(INTDIR)\ToolTipDlg.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\01Sniffer.bsc"
	-@erase "$(OUTDIR)\01Sniffer.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\01Sniffer.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\01Sniffer.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\01Sniffer.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\EncDec.sbr" \
	"$(INTDIR)\MD5.sbr" \
	"$(INTDIR)\01Sniffer.sbr" \
	"$(INTDIR)\01SnifferDlg.sbr" \
	"$(INTDIR)\SHA1.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\ToolTipDlg.sbr"

"$(OUTDIR)\01Sniffer.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\01Sniffer.pdb" /machine:I386 /out:"$(OUTDIR)\01Sniffer.exe" 
LINK32_OBJS= \
	"$(INTDIR)\EncDec.obj" \
	"$(INTDIR)\MD5.obj" \
	"$(INTDIR)\01Sniffer.obj" \
	"$(INTDIR)\01SnifferDlg.obj" \
	"$(INTDIR)\SHA1.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\ToolTipDlg.obj" \
	"$(INTDIR)\01Sniffer.res"

"$(OUTDIR)\01Sniffer.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "01Sniffer - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# 开始自定义宏
OutDir=.\Debug
# 结束自定义宏

ALL : "$(OUTDIR)\01Sniffer.exe" "$(OUTDIR)\01Sniffer.bsc"


CLEAN :
	-@erase "$(INTDIR)\EncDec.obj"
	-@erase "$(INTDIR)\EncDec.sbr"
	-@erase "$(INTDIR)\MD5.obj"
	-@erase "$(INTDIR)\MD5.sbr"
	-@erase "$(INTDIR)\01Sniffer.obj"
	-@erase "$(INTDIR)\01Sniffer.pch"
	-@erase "$(INTDIR)\01Sniffer.res"
	-@erase "$(INTDIR)\01Sniffer.sbr"
	-@erase "$(INTDIR)\01SnifferDlg.obj"
	-@erase "$(INTDIR)\01SnifferDlg.sbr"
	-@erase "$(INTDIR)\SHA1.obj"
	-@erase "$(INTDIR)\SHA1.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\ToolTipDlg.obj"
	-@erase "$(INTDIR)\ToolTipDlg.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\01Sniffer.bsc"
	-@erase "$(OUTDIR)\01Sniffer.exe"
	-@erase "$(OUTDIR)\01Sniffer.ilk"
	-@erase "$(OUTDIR)\01Sniffer.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\01Sniffer.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\01Sniffer.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\01Sniffer.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\EncDec.sbr" \
	"$(INTDIR)\MD5.sbr" \
	"$(INTDIR)\01Sniffer.sbr" \
	"$(INTDIR)\01SnifferDlg.sbr" \
	"$(INTDIR)\SHA1.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\ToolTipDlg.sbr"

"$(OUTDIR)\01Sniffer.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\01Sniffer.pdb" /debug /machine:I386 /out:"$(OUTDIR)\01Sniffer.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\EncDec.obj" \
	"$(INTDIR)\MD5.obj" \
	"$(INTDIR)\01Sniffer.obj" \
	"$(INTDIR)\01SnifferDlg.obj" \
	"$(INTDIR)\SHA1.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\ToolTipDlg.obj" \
	"$(INTDIR)\01Sniffer.res"

"$(OUTDIR)\01Sniffer.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("01Sniffer.dep")
!INCLUDE "01Sniffer.dep"
!ELSE 
!MESSAGE Warning: cannot find "01Sniffer.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "01Sniffer - Win32 Release" || "$(CFG)" == "01Sniffer - Win32 Debug"
SOURCE=.\EncDec.CPP

"$(INTDIR)\EncDec.obj"	"$(INTDIR)\EncDec.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"


SOURCE=.\MD5.cpp

"$(INTDIR)\MD5.obj"	"$(INTDIR)\MD5.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"


SOURCE=.\01Sniffer.cpp

"$(INTDIR)\01Sniffer.obj"	"$(INTDIR)\01Sniffer.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"


SOURCE=.\01Sniffer.rc

"$(INTDIR)\01Sniffer.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\01SnifferDlg.cpp

"$(INTDIR)\01SnifferDlg.obj"	"$(INTDIR)\01SnifferDlg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"


SOURCE=.\SHA1.cpp

"$(INTDIR)\SHA1.obj"	"$(INTDIR)\SHA1.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"


SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "01Sniffer - Win32 Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\01Sniffer.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\01Sniffer.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "01Sniffer - Win32 Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\01Sniffer.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\01Sniffer.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\ToolTipDlg.cpp

"$(INTDIR)\ToolTipDlg.obj"	"$(INTDIR)\ToolTipDlg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\01Sniffer.pch"



!ENDIF 

