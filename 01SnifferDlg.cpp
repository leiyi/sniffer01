// 01SnifferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "01Sniffer.h"
#include "01SnifferDlg.h"
#include "AboutDlg.h"
#include "EncodeDecode.h"
#include <string.h>
#include <winnls.h>
#include <ctype.h>
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// C01SnifferDlg dialog

void	C01SnifferDlg::DoDataExchange(CDataExchange* pDX)
{
	CToolTipDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(C01SnifferDlg)
	DDX_Control(pDX, IDC_COMBO_UNICODE,		m_Combo_UniCode);
	DDX_Control(pDX, IDC_EDIT_UTF8,			m_Edit_UTF8);
	DDX_Control(pDX, IDC_EDIT_ASCII,		m_Edit_ASCII);
	DDX_Control(pDX, IDC_CHECK_CUT0,		m_Button_Cut0);
	DDX_Control(pDX, IDC_CHECK_SIGN,		m_Button_Sign);
	DDX_Control(pDX, IDC_EDIT_BASE64,		m_Edit_Base64);
	DDX_Control(pDX, IDC_EDIT_HEX,			m_Edit_Hex);
	DDX_Control(pDX, IDC_EDIT_HEX2,			m_Edit_Hex2);
	DDX_Control(pDX, IDC_CHECK_XU,			m_Button_XU);
	DDX_Control(pDX, IDC_EDIT_64,			m_Edit_64);
	DDX_Control(pDX, IDC_EDIT_32,			m_Edit_32);
	DDX_Control(pDX, IDC_EDIT_16,			m_Edit_16);
	DDX_Control(pDX, IDC_EDIT_10,			m_Edit_10);
	DDX_Control(pDX, IDC_EDIT_8,			m_Edit_8);
	DDX_Control(pDX, IDC_EDIT_2,			m_Edit_2);
	DDX_Control(pDX, IDC_EDIT_RADIX,		m_Edit_Radix);
	DDX_Control(pDX, IDC_EDIT_RADIXVALUE,	m_Edit_RadixValue);
	DDX_Control(pDX, IDC_EDIT_MD5,			m_Edit_MD5);
	DDX_Control(pDX, IDC_EDIT_MD5B64,		m_Edit_MD5B64);
	DDX_Control(pDX, IDC_EDIT_SHA1,			m_Edit_SHA1);
	DDX_Control(pDX, IDC_EDIT_BYTENUMSTR,	m_Edit_ByteNumStr);
	DDX_Control(pDX, IDC_EDIT_BYTENUMINT,	m_Edit_ByteNumInt);
	DDX_Control(pDX, IDC_STATIC_ERRSTR,		m_Text_ErrString);
	DDX_Control(pDX, IDC_STATIC_ERRINT,		m_Text_ErrInt);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(C01SnifferDlg, CToolTipDlg)
	//{{AFX_MSG_MAP(C01SnifferDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CHAR()
	ON_CBN_EDITCHANGE(IDC_COMBO_UNICODE,	OnChangeComboUnicode)
	ON_CBN_SELCHANGE(IDC_COMBO_UNICODE, OnSelchangeComboUnicode)
	ON_EN_CHANGE(IDC_EDIT_UTF8,	OnChangeEditUtf8)
	ON_EN_CHANGE(IDC_EDIT_ASCII,	OnChangeEditAscii)
	ON_BN_CLICKED(IDC_CHECK_CUT0,	OnCheckCut0)
	ON_BN_CLICKED(IDC_CHECK_SIGN,	OnCheckSign)
	ON_EN_CHANGE(IDC_EDIT_BASE64,	OnChangeEditBase64)
	ON_EN_CHANGE(IDC_EDIT_HEX,	OnChangeEditHex)
	ON_EN_CHANGE(IDC_EDIT_HEX2,	OnChangeEditHex2)
	ON_BN_CLICKED(IDC_CHECK_XU,	OnCheckXu)
	ON_EN_CHANGE(IDC_EDIT_64,	OnChangeEdit64)
	ON_EN_CHANGE(IDC_EDIT_32,	OnChangeEdit32)
	ON_EN_CHANGE(IDC_EDIT_16,	OnChangeEdit16)
	ON_EN_CHANGE(IDC_EDIT_10,	OnChangeEdit10)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_10, OnDeltaposSpin10)
	ON_EN_CHANGE(IDC_EDIT_8,	OnChangeEdit8)
	ON_EN_CHANGE(IDC_EDIT_2,	OnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT_RADIX,	OnChangeEditRadix)
	ON_EN_KILLFOCUS(IDC_EDIT_RADIX,	OnKillfocusEditRadix)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RADIX, OnDeltaposSpinRadix)
	ON_EN_CHANGE(IDC_EDIT_RADIXVALUE,	OnChangeEditRadixValue)
	ON_BN_CLICKED(ID_BUTTON_CLEAR,	OnButtonClear)
	ON_WM_CTLCOLOR()
	ON_WM_HELPINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

C01SnifferDlg::C01SnifferDlg(CWnd* pParent) : CToolTipDlg(C01SnifferDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(C01SnifferDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nHistoryNum = 0;
	for(int i=0; i<MAX_HISTORY_NUM; i++)
		m_pArHistory[i] = NULL;
	m_bSelfChange = TRUE;	//置True，确保MFC库控件初始化过程中也触发 OnChangexxx() 系列时不致于关联死循环。
}

C01SnifferDlg::~C01SnifferDlg()
{
//	SaveIni(); 在 Debug Unicode 模式下运行时发现此时调用已晚，m_nHistoryNum 已被破坏导致 Access vialation，但 release 版本没有vialation。Why？
	for(int i=0; i<MAX_HISTORY_NUM; i++)
	{
		CString* pStr = m_pArHistory[i];
		if(pStr)
			delete pStr;
	}
}

BOOL	C01SnifferDlg::OnInitDialog()
{
	CToolTipDlg::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// GetPrivateProfileString

	LoadIni();

	m_bSelfChange = FALSE;	//使得修改输入框的关联触发生效，等候用户输入

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//加载Ini数据
void	C01SnifferDlg::LoadIni()
{
	m_oEncDec.m_bCut0 = GetPrivateProfileInt(INI_APP_CONFIG, INI_KEY_CUT0, 0, INI_FILENAME) ? TRUE : FALSE;
	m_Button_Cut0.SetCheck(m_oEncDec.m_bCut0);

	m_oEncDec.m_bSign = GetPrivateProfileInt(INI_APP_CONFIG, INI_KEY_SIGNED, 0, INI_FILENAME) ? TRUE : FALSE;
	m_Button_Sign.SetCheck(m_oEncDec.m_bSign);

	m_oEncDec.m_bXUType = GetPrivateProfileInt(INI_APP_CONFIG, INI_KEY_XU, 0, INI_FILENAME) ? TRUE : FALSE;
	m_Button_XU.SetCheck(m_oEncDec.m_bXUType);

	m_oEncDec.SetRadix(GetPrivateProfileInt(INI_APP_CONFIG, INI_KEY_RADIX, 63, INI_FILENAME));	//63: 首次运行时选63进制
	m_Edit_Radix.SetWindowText(m_oEncDec.m_csRadix);	//显示进制基数

	LoadHistory();
}


static TCHAR* DemoHistorys[] = {
	_T("2D4E3100"),													//中1
	_T("C1AACDA820D2C6B6AF"),										//联通 移动 GB2312
	_T("2D4E8765417ED49A"),											//中文繁體
	_T("F182ED8B3A0045006E0067006C00690073006800"),					//英语:English
	_T("E565ED8B3A00B830E330D130F330"),								//日语
	_T("E997ED8B3A005CD56DAD58C7"),									//韩语
	_T("C44F87653A0040044304410441043A0438043904"),					//俄文
	_T("0C5E4A81ED8B3A00B503BB03BB03B703BD03B903BA03AC03"),			//希腊语
	_T("3F96C9622F4F3A003906310628064A06"),							//阿拉伯
	_T("6C9A76517F983A001C0430043A04350434043E043D04380458043004"),	//马其顿
	_T("F06C6253FA56ED8B3A00240C460C320C410C170C410C")				//泰卢固语
};
const int DEMO_HISTORY_NUM = sizeof(DemoHistorys)/sizeof(TCHAR*);
#if DEMO_HISTORY_NUM > MAX_HISTORY_NUM
#error ("Too many history strings defined! 预定义的示范历史个数超限");
#endif

//初始化默认的示范例子字符串：“中文”、“英语:English”等
void	C01SnifferDlg::LoadHistory()
{
	//从 INI 文件恢复历史
	CString key;
	TCHAR	value[MAX_HISTORY_BYTES+1];
	for(int i=0; i<MAX_HISTORY_NUM; i++)
	{
		key.Format(_T("%d"), i);
		GetPrivateProfileString(INI_APP_HISTORY, key, _T(""), value, MAX_HISTORY_BYTES, INI_FILENAME);
		if(!LoadOneHistoryItem(i, value))
			break;	//INI 中的历史读完
	}

	//首次运行，或者可能INI中的历史记录被删过，则添加示范例子串作为历史
	if(m_nHistoryNum < DEMO_HISTORY_NUM)
		AddDemoHistory();

	//用第0项显示，恢复上次运行的现场
	InitHistory0();
}

//增加示范例子字符串：“中文”、“英语:English”等，尽量装够 MAX_HISTORY_NUM 个 
void	C01SnifferDlg::AddDemoHistory()
{
	int nIniHistoryCount = m_Combo_UniCode.GetCount();	//从 INI 中已经读出的历史个数
	for(int i=0; i<MAX_HISTORY_NUM-nIniHistoryCount,i<DEMO_HISTORY_NUM; i++)
		LoadOneHistoryItem(i, DemoHistorys[i]);
}

//把一项INI中的历史或者预定义的示范历史恢复到 Unicode 下拉框历史中
//返回：恢复内容的有效字节数
//如果 INI 中的历史被它处编辑成无法进行HEX解析的非法内容，则将返回0，由调用者分析情况并处理
int	C01SnifferDlg::LoadOneHistoryItem(int nIndex, LPCTSTR pStr)
{
	CString* newStr=new CString(pStr);
	m_oEncDec.m_csHex = *newStr;
	m_oEncDec.HexEditToM();
	if(m_oEncDec.m_nBytesLen> 0)
	{
		m_pArHistory[m_nHistoryNum++] = newStr;
		m_oEncDec.MToUnicodeEdit();
		m_Combo_UniCode.InsertString(nIndex, m_oEncDec.m_csUnicode);	//m_Combo_UniCode.AddString(m_oEncDec.m_csUnicode);
	}

	return m_oEncDec.m_nBytesLen;
}

//用第一项历史预设到当前显示
void	C01SnifferDlg::InitHistory0()
{
	m_bSelfChange = FALSE;	//使得修改输入框的关联触发生效
	m_Edit_Hex.SetWindowText(*m_pArHistory[0]);
}

//按编码后的新值，显示各框。
void	C01SnifferDlg::Display(unsigned short nCallerId)
{
	m_bSelfChange = TRUE;	//置自调用修改模式标志，以便调用SetWindowText时禁止OnChangeEditxxx()系列函数执行关联解码计算，避免无限递归

	//根据被修改框，刷新其它的框，文本已被转换在 pEncDec 的相应成员中
	//不能 SetWindowText 当期修改框，否则光标被设回开头，影响编辑
	if(nCallerId != CallbyUnicode)		m_Combo_UniCode.SetWindowText	(m_oEncDec.m_csUnicode	);
	if(nCallerId != CallbyUTF8)			m_Edit_UTF8.SetWindowText		(m_oEncDec.m_csUtf8		);
	if(nCallerId != CallbyAscii)		m_Edit_ASCII.SetWindowText		(m_oEncDec.m_csAscii	);
	if(nCallerId != CallbyBase64)		m_Edit_Base64.SetWindowText		(m_oEncDec.m_csBase64	);
	if(nCallerId != CallbyHex)			m_Edit_Hex.SetWindowText		(m_oEncDec.m_csHex		);
	if(nCallerId != CallbyHex2)			m_Edit_Hex2.SetWindowText		(m_oEncDec.m_csHex2		);
	if(nCallerId != Callby64)			m_Edit_64.SetWindowText			(m_oEncDec.m_cs_64		);
	if(nCallerId != Callby32)			m_Edit_32.SetWindowText			(m_oEncDec.m_cs_32		);
	if(nCallerId != Callby16)			m_Edit_16.SetWindowText			(m_oEncDec.m_cs_16		);
	if(nCallerId != Callby10)			m_Edit_10.SetWindowText			(m_oEncDec.m_cs_10		);
	if(nCallerId != Callby8)			m_Edit_8.SetWindowText			(m_oEncDec.m_cs_8		);
	if(nCallerId != Callby2)			m_Edit_2.SetWindowText			(m_oEncDec.m_cs_2		);
	if(nCallerId != CallbyRadixValue)	m_Edit_RadixValue.SetWindowText	(m_oEncDec.m_csRadixValue);

	// 不可逆的三个编码，只会随可逆编码的改动而改动：
	m_Edit_MD5.SetWindowText(m_oEncDec.m_csMd5);
	m_Edit_MD5B64.SetWindowText(m_oEncDec.m_csMd5Base64);
	m_Edit_SHA1.SetWindowText(m_oEncDec.m_csSha1);

	TCHAR chStrLen[12];
	ItoA(m_oEncDec.m_nBytesLen, chStrLen);
	m_Edit_ByteNumStr.SetWindowText(chStrLen);	//显示字符串的字节数
	ItoA(m_oEncDec.m_nInt64Len, chStrLen);
	m_Edit_ByteNumInt.SetWindowText(chStrLen);	//显示数值的字节数
	DisplayError(nCallerId);

	m_bSelfChange = FALSE;	//恢复关联解码计算
}

void	AppendErr(LPTSTR ErrBuffer, LPTSTR Err)
{
	if (!ErrBuffer) return;
	if(_tcslen(ErrBuffer))
		_tcscat(ErrBuffer, _T("、"));
	_tcscat(ErrBuffer, Err);
}

//显示输入内容的合法性报告
//设计需求：
//	1、错误分格式错误、非法字符、长度溢出以及字符串数据超过INT64的8字节限制四种，可同时出现多种；
//	2、前三种错误在字符串区和数据区都存在，哪个区出现错误就提示在哪个区，第四种错误发生在修改字符串区时，但提示在数值区
//	3、错误显示红色，正确显示绿色“正确”二字
/*
#define ERROR_NONE		0	//清除
#define ERROR_FORMAT		0x1	//格式错误，比如\x错误。
#define ERROR_INVALIDCHAR	0x2	//非法字符
#define ERROR_OVERFLOW		0x4	//字符串区长度超过缓冲区MAX_BYTE限制，或者数值区字符串转数据超过INT64限制
#define ERROR_CUTSTRINGTO8	0x8	//字符串数据超过8字节，只截取前8字节作为INT
*/
void	C01SnifferDlg::DisplayError(unsigned short nCallerId)
{
	TCHAR	Err_String[32]={0}, Err_Data[32]={0};
	TCHAR	*Err_Prompts;
	unsigned err = m_oEncDec.m_nErr;
	Err_Prompts = NULL;
	if((nCallerId & CallbyStringGroup) == CallbyStringGroup)
		Err_Prompts = &Err_String[0];
	else if((nCallerId & CallbyIntGroup) == CallbyIntGroup)
		Err_Prompts = &Err_Data[0];


	if( (err & ( ERROR_FORMAT | ERROR_INVALIDCHAR | ERROR_OVERFLOW )) == ERROR_NONE)
	{
		AppendErr(Err_Prompts, _T("正确"));
		m_ErrColor = RGB(0,127,0);		//绿色
	}
	else
	{
		m_ErrColor = RGB(255,0,0);		//红色
		if(err & ERROR_FORMAT) AppendErr(Err_Prompts, _T("格式错误"));
		if(err & ERROR_INVALIDCHAR) AppendErr(Err_Prompts, _T("非法字符"));
		if(err & ERROR_OVERFLOW) AppendErr(Err_Prompts, _T("长度溢出"));
	}

	if(err & ERROR_CUTSTRINGTO8) AppendErr(Err_Data, _T("数值仅取前8字节"));

	m_Text_ErrString.SetWindowText(Err_String);
	m_Text_ErrInt.SetWindowText(Err_Data);
}

//当某一编辑框有变动时的统一处理函数
//（经断点跟踪，发现触发者包括：MFC库初始化各编辑控件时、InitHistory0()恢复时、用户修改时、被其它框关联执行SetWindowText()时，据此维护好 m_bSelfChange 赋值）
void	C01SnifferDlg::OnChangeEdit(int nCallerId)
{
	if(m_bSelfChange)	//如果由其它编辑框动作触发修改，则不能执行关联解码处理，否则将导致循环触发。
		return;
	
	static struct EditItem{int CallerId; CWnd* pCWnd;} editArray[]=	{	//每条对应一个ID、一个用户可编辑的控件
		{CallbyUnicode,		&m_Combo_UniCode	},
		{CallbyUTF8,		&m_Edit_UTF8		},
		{CallbyAscii,		&m_Edit_ASCII		},
		{CallbyBase64,		&m_Edit_Base64		},
		{CallbyHex,			&m_Edit_Hex			},
		{CallbyHex2,		&m_Edit_Hex2		},
		{Callby64,			&m_Edit_64			},
		{Callby32,			&m_Edit_32			},
		{Callby16,			&m_Edit_16			},
		{Callby10,			&m_Edit_10			},
		{Callby8,			&m_Edit_8			},
		{Callby2,			&m_Edit_2			},
		{CallbyRadixValue,	&m_Edit_RadixValue	}
	};
	for(int i=0; i<sizeof(editArray)/sizeof(struct EditItem); i++)
	{
		struct EditItem &aItem = editArray[i];
		if(aItem.CallerId == nCallerId)
		{
			aItem.pCWnd->GetWindowText(m_csChangedString);	//获得当前修改框中的字符串
			m_oEncDec.EditChanged(m_csChangedString, nCallerId);
			Display(nCallerId);			//更新其它格式框中的显示
			return;
		}
	}

	ASSERT(0);	//不应该执行到此。漏定义 editArray 的话则触发
}

//以下 OnChangeCombxxx OnChangeEditxxx 等系列 MFC 回调函数，分别是各个输入框中内容有变动时（包括用户修改和被其它框关联修改）的MFC回调入口。

void	C01SnifferDlg::OnChangeComboUnicode()
{
	OnChangeEdit(CallbyUnicode);
}

//Unicode 编辑框被下拉选择修改时。包括鼠标点击选择或者方向键改变选项时
//注：此时显示尚未更新，因此 GetWindowText()获取不到新选项的文本。参见 MSDN CComboBox 的说明：“ON_CBN_SELCHANGE   The selection in the list box of a combo box is about to be changed as a result of the user either clicking in the list box or changing the selection by using the arrow keys. When processing this message, the text in the edit control of the combo box can only be retrieved via GetLBText or another similar function. GetWindowText cannot be used.”
void C01SnifferDlg::OnSelchangeComboUnicode() 
{
	// TODO: Add your control notification handler code here
	int index = m_Combo_UniCode.GetCurSel();
	m_Combo_UniCode.GetLBText(index, m_csChangedString);
	m_oEncDec.EditChanged(m_csChangedString, CallbyUnicode);
	Display(CallbyUnicode);
}

void	C01SnifferDlg::OnChangeEditUtf8()
{
	OnChangeEdit(CallbyUTF8);
}

void	C01SnifferDlg::OnChangeEditAscii()
{
	OnChangeEdit(CallbyAscii);
}
void C01SnifferDlg::OnChangeEditBase64()
{
	OnChangeEdit(CallbyBase64);
}

void	C01SnifferDlg::OnChangeEditHex()
{
	OnChangeEdit(CallbyHex);
}

void	C01SnifferDlg::OnChangeEditHex2()
{
	OnChangeEdit(CallbyHex2);
}

void	C01SnifferDlg::OnChangeEdit64()
{
	OnChangeEdit(Callby64);
}

void	C01SnifferDlg::OnChangeEdit32()
{
	OnChangeEdit(Callby32);
}

void	C01SnifferDlg::OnChangeEdit16()
{
	OnChangeEdit(Callby16);
}

void	C01SnifferDlg::OnChangeEdit10()
{
	OnChangeEdit(Callby10);
}

void	C01SnifferDlg::OnDeltaposSpin10(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_oEncDec.DeltaposSpin10(pNMUpDown->iDelta>0 ? -1 : 1 );
	Display(CallbyOther);
	*pResult = 1;
}

void	C01SnifferDlg::OnChangeEdit8()
{
	OnChangeEdit(Callby8);
}

void	C01SnifferDlg::OnChangeEdit2()
{
	OnChangeEdit(Callby2);
}

void	C01SnifferDlg::OnChangeEditRadix()
{
	//编辑时不宜触发进制变更处理，待离开焦点时触发最合适
}

//手工输入进制计数编辑框以后离开焦点时的处理方法
void	C01SnifferDlg::OnKillfocusEditRadix()
{
	// TODO: Add your control notification handler code here
	CString csRadix;
	m_Edit_Radix.GetWindowText(csRadix);
	SetRadix(AtoI(csRadix));
}

void	C01SnifferDlg::OnDeltaposSpinRadix(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
/* UDN_DELTAPOS
    lpnmud = (LPNMUPDOWN) lParam;
    lpnmud:
	Address of an NMUPDOWN structure that contains information about the position change.
	The iPos member of this structure contains the current position of the control. The iDelta member of the structure is a signed integer that contains the proposed change in position. If the user has clicked the up button, this is a positive value. If the user has clicked the down button, this is a negative value.
    Return Values:
	Return nonzero to prevent the change in the control's position, or zero to allow the change.
    Remarks
	The UDN_DELTAPOS notification is sent before the WM_VSCROLL or WM_HSCROLL message, which actually changes the control's position. This lets you examine, allow, modify, or disallow the change.
*/
	UINT8 nNewRadix = m_oEncDec.m_nRadix;
	if(pNMUpDown->iDelta > 0)	//down button
		nNewRadix --;
	else
		nNewRadix ++;
	SetRadix(nNewRadix);
	m_Edit_Radix.SetWindowText(m_oEncDec.m_csRadix);	//显示进制框
	*pResult = 1;
}

//按编辑框或spin按钮修改进制（可能因超范围被修改到默认范围），并立即修改新进制下的RadixValue框的显示
void	C01SnifferDlg::SetRadix(UINT8 nNewRadix)
{
	m_oEncDec.SetRadix(nNewRadix);
	m_oEncDec.RecalculateRadix();
	m_bSelfChange = TRUE;	//控制只修改RadixValue框显示，不要触发更多的关联编码计算动作。
	m_Edit_RadixValue.SetWindowText(m_oEncDec.m_csRadixValue);
	m_bSelfChange = FALSE;
}

void	C01SnifferDlg::OnChangeEditRadixValue()
{
	OnChangeEdit(CallbyRadixValue);
}

//按动“±符号”开关时，切换标志，并立即刷新各数值框
void	C01SnifferDlg::OnCheckSign()
{
	m_oEncDec.SwitchSign();	//OnCheckSign(m_Button_Sign.GetCheck()); 不必取按钮状态，由 m_oEncDec.SwitchSign() 根据 m_bSign 判断也可
	Display(CallbyOther);			//所有框都刷新，其实只有数值区有变化
}

//按动“截0”开关时，切换标志，并立即修改Ascii、Unicode 和 Utf8 框。
void	C01SnifferDlg::OnCheckCut0()
{
	if(m_Button_Cut0.GetCheck())
		m_oEncDec.m_bCut0=TRUE;
	else
		m_oEncDec.m_bCut0=FALSE;
	m_oEncDec.MToAsciiEdit();
	m_oEncDec.MToUnicodeEdit();
	m_oEncDec.MToUtf8Edit();

	Display(CallbyOther);
}

void	C01SnifferDlg::SetCheckXU(bool bNewStatus)
{
	m_oEncDec.m_bXUType = bNewStatus;
	m_oEncDec.MtoHex2Edit();
	m_bSelfChange = TRUE;	//控制只修改Hex2框显示，不要触发更多的关联编码计算动作。
	m_Edit_Hex2.SetWindowText(m_oEncDec.m_csHex2);
	m_bSelfChange = FALSE;
}

//按动“\x\u”开关时，切换\x \u 转移符，并立即修改 IDC_STATIC_HEX2 静态文本。
void	C01SnifferDlg::OnCheckXu()
{
	// TODO: Add your control notification handler code here
	SetCheckXU(m_Button_XU.GetCheck() ? TRUE : FALSE);
}

//按动“清空 Clear”按钮后的响应
void	C01SnifferDlg::OnButtonClear()
{
	// TODO: Add your control notification handler code here
	m_oEncDec.ButtonClear();
	Display(CallbyOther);				//所有框都刷新
	m_Combo_UniCode.SetFocus();			//焦点转到Unicode框，可选的动作，搞忘了不作这个会如何
}

//自定义 OnOk 和 OnCancel ，取消它程序主窗口的性质，使得 SaveIni() 中的错误提示窗口能显示。否则DoModal结束后应用程序收到WM_QUIT而退出消息循环，后面的对话框就不会显示了
void	C01SnifferDlg::OnOK()
{
	EndMyDialog();
	CToolTipDlg::OnOK();
}
void	C01SnifferDlg::OnCancel()
{
	EndMyDialog();
	CToolTipDlg::OnCancel();
}
void	C01SnifferDlg::EndMyDialog()
{
	SaveIni();
	AfxGetApp()->m_pMainWnd = NULL;
}

//保存INI文件。返回 FALSE 表示不成功，比如无写文件权限
BOOL	C01SnifferDlg::SaveIni()
{
	TCHAR chBool[2];
	ItoA(m_oEncDec.m_bCut0, chBool);
	if(!WritePrivateProfileString(INI_APP_CONFIG, INI_KEY_CUT0, chBool, INI_FILENAME))
	{
		TCHAR strCurDir[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, strCurDir);
		CString csErr;
		csErr.Format(_T("　　在 %s 目录保存 INI 文件失败！可能是因为没有权限或者磁盘空间满的原因……\n\n　　注：INI 文件用于保存历史转换记录、当前设置的数进制、是否截0等使用习惯数据。"), strCurDir);
		AfxMessageBox(csErr, MB_ICONWARNING);
		return FALSE;
	}
	ItoA(m_oEncDec.m_bXUType, chBool);
	WritePrivateProfileString(INI_APP_CONFIG, INI_KEY_XU, chBool, INI_FILENAME);

	ItoA(m_oEncDec.m_bSign, chBool);
	WritePrivateProfileString(INI_APP_CONFIG, INI_KEY_SIGNED, chBool, INI_FILENAME);

	WritePrivateProfileString(INI_APP_CONFIG, INI_KEY_RADIX, m_oEncDec.m_csRadix, INI_FILENAME);

	CString key;
	CString value;
	for(int i=0; i<m_nHistoryNum; i++)
	{
		key.Format(_T("%d"), i);
		WritePrivateProfileString(INI_APP_HISTORY, key,	*m_pArHistory[i], INI_FILENAME);
	}

	return TRUE;
}

//重载以处理 ESC Enter 键。当不拦截时，ESC 和 Enter 将结束 DoModal() 并返回 IDOK，然后退出对话框。
BOOL	C01SnifferDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN)
	{
		//阻止 ESC 键
		if(pMsg->wParam==VK_ESCAPE)
			return TRUE;

		//拦截 Enter 按键
		if(pMsg->wParam == VK_RETURN)
		{
			if(!m_Combo_UniCode.GetDroppedState())
			{//当 Unicode Combox 下拉框在未展开状态时按 Enter 键，进行保存历史处理，且编辑效果上使光标移到最后
				pMsg->wParam = VK_END;	//改为按 End 键处理	//pMsg->wParam = VK_TAB;	//改为按 TAB 键处理
				TrySaveHistroy();
			}
		}
	}

//	if (pMsg->message == WM_QUIT)	此测试用于了解 Windows 消息机制。结果：此处没有 WM_QUIT 消息。
//		MessageBox(_T("即将退出"), _T("测试"));
	return CToolTipDlg::PreTranslateMessage(pMsg);
}

//（编辑框中按Enter键时）保存一项历史（但暂不存入文件）
void	C01SnifferDlg::TrySaveHistroy()
{
	if(m_oEncDec.m_nBytesLen > MAX_HISTORY_BYTES)
	{
		CString err;
		err.Format(_T("“%s”长度为%d字节，超过%d，无法保存"), m_oEncDec.m_csUnicode, m_oEncDec.m_nBytesLen, MAX_HISTORY_BYTES);
		MessageBox(err, _T("提示"));
	}
	else if(m_oEncDec.m_nBytesLen > 0 )
	{
		BOOL bNew = TRUE;	//假定历史中没有
		for (int i=0; i<m_nHistoryNum; i++)
			if(m_pArHistory[i]->Compare(m_oEncDec.m_csHex) == 0) {bNew = FALSE; break;}
		if(bNew)
		{
			if(m_pArHistory[MAX_HISTORY_NUM-1])
			{
				delete m_pArHistory[MAX_HISTORY_NUM-1];			//满了，则删除1项
				if(m_Combo_UniCode.GetCount() == MAX_HISTORY_NUM)
					m_Combo_UniCode.DeleteString(MAX_HISTORY_NUM-1);
			}
			else
				m_nHistoryNum++;
			for(i=MAX_HISTORY_NUM-1; i>0; i--)
				m_pArHistory[i] = m_pArHistory[i-1];			//往后挪
			m_pArHistory[0] = new CString(m_oEncDec.m_csHex);	//保存
			m_Combo_UniCode.InsertString(0, m_oEncDec.m_csUnicode);
			CString saved;
			saved.Format(_T("“%s”已经保存到历史中"), m_csChangedString);
			MessageBox(saved, _T("提示"));			
		}
	}
}

//重载以按 F1 后进入网页帮助。不处理的话，提示“启动帮助失败!”
BOOL	C01SnifferDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: Add your message handler code here and/or call default
	CString csWebAddress;
	csWebAddress.LoadString(IDS_HOMEPAGE);
	CString csPrompt;
	csPrompt.Format(_T("本地没有帮助资料，将访问 %s 网页获取帮助"),csWebAddress.GetBuffer(0));
	MessageBox(csPrompt);
	ShellExecute(NULL, NULL, csWebAddress.GetBuffer(0), NULL, NULL, SW_SHOWNORMAL);
	return TRUE;
	//return CToolTipDlg::OnHelpInfo(pHelpInfo);
}

HBRUSH	C01SnifferDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CToolTipDlg::OnCtlColor(pDC, pWnd, nCtlColor);
	// TODO: Change any attributes of the DC here

	if(pWnd->GetDlgCtrlID()==IDC_STATIC_ERRSTR || pWnd->GetDlgCtrlID()==IDC_STATIC_ERRINT)
		pDC->SetTextColor(m_ErrColor);
	else
		pDC->SetTextColor(RGB(0,0,0));

	// TODO: Return a different brush if the default is not desired
	return hbr;
}

void	C01SnifferDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CToolTipDlg::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void	C01SnifferDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CToolTipDlg::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR	C01SnifferDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void	C01SnifferDlg::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	CToolTipDlg::OnChar(nChar, nRepCnt, nFlags);
}

