// MyString.h 自定义的字符串函数
//////////////////////////////////////////////////////////////////////

#if !defined(MYSTRING_INCLUDED_)
#define MYSTRING_INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifdef _UNICODE
int		MBCSPtrToUnicodeStr(CString& dest, LPCSTR src, int srcLen = -1);	//按 MBCS 缓冲区指针得到 Unicode CString，用于 _UNICODE 编译环境。如果 src 不含中0，则可忽略 srcLen 参数

#else
int		UnicodePtrToMBCSStr(CString& dest, LPCWSTR src, int srcLen);
#endif

int		Utf8ToStr(CString& dest, LPCSTR m_pBytes, int srcLen);
void	SetCStringContent(CString& str, LPCTSTR buffer, int len);
#endif // !defined(MYSTRING_INCLUDED_)

