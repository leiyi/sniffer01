// 01SnifferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
IMPLEMENT_DYNCREATE(CAboutDlg, CDialog)

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void	CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_STATIC_HOMEPAGE, m_Static_HomePage);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_STATIC_HOMEPAGE, OnGotoHomePage)
	ON_WM_CTLCOLOR()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void	CAboutDlg::OnGotoHomePage()
{
	ShellExecute(NULL, NULL, m_csWebAddress.GetBuffer(0), NULL, NULL, SW_SHOWNORMAL);
}

HBRUSH	CAboutDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	// TODO: Change any attributes of the DC here
	
	// TODO: Return a different brush if the default is not desired
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    pDC->SetBkMode(TRANSPARENT);
	CBrush brSolid;
	brSolid.CreateSolidBrush(RGB(194,233,241));
	
	switch (pWnd->GetDlgCtrlID())
	{
	case IDC_STATIC_HOMEPAGE:
		pDC->SetTextColor(RGB(0,0,255));
		pDC->SetBkMode(TRANSPARENT);
		break;
	default:
		break;
	}
	return hbr;      //创建背景刷子
}

//设置超链接鼠标形状。每次鼠标（在当前对话框区域内）移动时被回调，频度非常高
BOOL	CAboutDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: Add your message handler code here and/or call default
#ifndef IDC_HAND
#define IDC_HAND MAKEINTRESOURCE(32649)
#endif
	int ctrlId = pWnd->GetDlgCtrlID();
	switch(ctrlId)
	{
	case IDC_STATIC_HOMEPAGE:
		SetCursor(LoadCursor(NULL,IDC_HAND));		//设置鼠标形状为手型
		break;
	default:
		SetCursor(LoadCursor(NULL,IDC_ARROW));		//设置鼠标形状为标准箭头状
		break;
	}
//	return CDialog::OnSetCursor(pWnd,nHitTest,message);
	return TRUE;
}

BOOL	CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_csWebAddress.LoadString(IDS_HOMEPAGE);
	m_Static_HomePage.SetWindowText(m_csWebAddress);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
