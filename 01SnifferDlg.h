// 01SnifferDlg.h : header file
//

#if !defined(AFX_01SNIFFERDLG_H__44ADCBC8_5182_48F8_99A3_C85E4165860C__INCLUDED_)
#define AFX_01SNIFFERDLG_H__44ADCBC8_5182_48F8_99A3_C85E4165860C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// C01SnifferDlg dialog
#include "ToolTipDlg.h"
#include <AFXEXT.H>

#include "EncodeDecode.h"

#define MAX_HISTORY_NUM		20
#define MAX_HISTORY_BYTES	36
#define INI_FILENAME	_T(".\\01Sniffer.ini")
#define INI_APP_HISTORY	_T("History")
#define INI_APP_CONFIG	_T("Config")
#define INI_KEY_CUT0	_T("cut0")
#define INI_KEY_XU		_T("xu")
#define INI_KEY_SIGNED	_T("signed")
#define INI_KEY_RADIX	_T("radix")

class C01SnifferDlg : public CToolTipDlg
{
// Construction
public:
	C01SnifferDlg(CWnd* pParent = NULL);	// standard constructor
	~C01SnifferDlg();

// Dialog Data
	//{{AFX_DATA(C01SnifferDlg)
	enum { IDD = IDD_MAIN_DIALOG };
	CComboBox	m_Combo_UniCode;
	CEdit	m_Edit_UTF8;
	CEdit	m_Edit_ASCII;
	CButton	m_Button_Cut0;
	CButton	m_Button_Sign;
	CEdit	m_Edit_Base64;
	CEdit	m_Edit_Hex;
	CEdit	m_Edit_Hex2;
	CButton	m_Button_XU;
	CEdit	m_Edit_64;
	CEdit	m_Edit_32;
	CEdit	m_Edit_16;
	CEdit	m_Edit_10;
	CEdit	m_Edit_8;
	CEdit	m_Edit_2;
	CEdit	m_Edit_Radix;
	CEdit	m_Edit_RadixValue;
	CEdit	m_Edit_MD5;
	CEdit	m_Edit_MD5B64;
	CEdit	m_Edit_SHA1;
	CEdit	m_Edit_ByteNumStr;
	CEdit	m_Edit_ByteNumInt;
	CStatic m_Text_ErrString;
	CStatic m_Text_ErrInt;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C01SnifferDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(C01SnifferDlg)
	virtual	void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChangeComboUnicode();
	afx_msg void OnSelchangeComboUnicode();
	afx_msg void OnChangeEditUtf8();
	afx_msg void OnChangeEditAscii();
	afx_msg void OnCheckCut0();
	afx_msg void OnCheckSign();
	afx_msg void OnChangeEditBase64();
	afx_msg void OnChangeEditHex();
	afx_msg void OnChangeEditHex2();
	afx_msg void OnCheckXu();
	afx_msg void OnChangeEdit64();
	afx_msg void OnChangeEdit32();
	afx_msg void OnChangeEdit16();
	afx_msg void OnChangeEdit10();
	afx_msg void OnDeltaposSpin10(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEdit8();
	afx_msg void OnChangeEdit2();
	afx_msg void OnChangeEditRadix();
	afx_msg void OnKillfocusEditRadix();
	afx_msg void OnDeltaposSpinRadix(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditRadixValue();
	afx_msg void OnButtonClear();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void	EndMyDialog();
	BOOL	SaveIni();				//保存INI文件。返回 FALSE 表示不成功，比如无写文件权限
	void	LoadIni();
	void	LoadHistory();			//重新加载历史字符串
	void	AddDemoHistory();		//增加演示例子字符串作为历史：“我是中文”、“I am English”等
	int		LoadOneHistoryItem(int nIndex, LPCTSTR pStr);	//设置 Unicode Combox 框的一行历史
	void	InitHistory0();
	void	TrySaveHistroy();		//（编辑框中按Enter键时）保存一项历史
	void	OnChangeEdit(int nCallerId);
	void	SetCheckXU(bool bNewStatus);
	void	SetRadix(UINT8 nNewRadix);

	void	Display(unsigned short nCallerId);
	void	DisplayError(unsigned short nCallerId);

	CEncodeDecode m_oEncDec;

	bool m_bSelfChange;				//控制各编辑框进行关联修改时只递归一轮，而避免无限触发致死。由各显示模块根据自己的场合赋值
	CString		m_csChangedString;	//最近被修改的编辑框中的字符串
	COLORREF	m_ErrColor;
	CString*	m_pArHistory[MAX_HISTORY_NUM];
	int		m_nHistoryNum;
};



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_01SNIFFERDLG_H__44ADCBC8_5182_48F8_99A3_C85E4165860C__INCLUDED_)
