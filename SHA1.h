// SHA1.h: interface for the CSHA1 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHA1_H__0585BB8A_73B8_4DBD_8E7E_16C7CED16A61__INCLUDED_)
#define AFX_SHA1_H__0585BB8A_73B8_4DBD_8E7E_16C7CED16A61__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSHA1  
{
public:
	CSHA1();
	virtual ~CSHA1();
	void	Reset();
	bool	Result(unsigned int *message_digest_array);
	void	Input(const unsigned char	*message_array,	unsigned int length);
	void	Input(	const char	*message_array,	unsigned int length);
	void	Input(unsigned char message_element);
	void	Input(char message_element);
	CSHA1& operator<<(const char *message_array);
	CSHA1& operator<<(const unsigned char *message_array);
	CSHA1& operator<<(const char message_element);
	CSHA1& operator<<(const unsigned char message_element);
	
private:
//	Process the next 512 bits of the message
	void	ProcessMessageBlock();
	/*
	*	Pads the current message block to 512 bits
	*/
	void	PadMessage();
	
	/*
	*	Performs a circular left shift operation
	*/
	inline unsigned	CircularShift(int bits, unsigned int word);
	
	char*	ClearZeroChar(char* inStr, int inLen, int& outLen);
	
	unsigned int H[5];					// Message digest buffers
	
	unsigned int Length_Low;				// Message length in bits
	unsigned int Length_High;				// Message length in bits
	
	unsigned char Message_Block[64];	// 512-bit message blocks
	int Message_Block_Index;			// Index into message block array
	
	bool Computed;						// Is the digest computed?
	bool Corrupted;						// Is the message digest corruped?

};

#endif // !defined(AFX_SHA1_H__0585BB8A_73B8_4DBD_8E7E_16C7CED16A61__INCLUDED_)
