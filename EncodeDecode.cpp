// EncDec.cpp: 编解码等公共函数
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EncodeDecode.h"
#include <ctype.h>
#include "MD5.h"
#include "SHA1.h"
#include "MyStdlib.h"
#include "MyString.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CEncodeDecode
//////////////////////////////////////////////////////////////////////

CEncodeDecode::CEncodeDecode()
{
	m_pBytes = NULL;
}

CEncodeDecode::~CEncodeDecode()
{
	TryFreeM();
}

void	CEncodeDecode::TryFreeM()
{
	if(m_pBytes)
	{
		delete m_pBytes;
		m_pBytes = NULL;
		m_nBytesLen = 0;
	}
}

bool	CEncodeDecode::TryNewM(int nNewLen)
{
	TryFreeM();
	m_pBytes = new char[nNewLen+1];
	bool ret;
	if (m_pBytes)
	{
		ret = TRUE;
		m_nBytesLen = nNewLen;
	}
	else
	{
		ret = FALSE;
		m_nBytesLen = 0;
		SetError(ERROR_OVERFLOW);
	}
	return ret;
}

//对以下错误：
//	“非法字符”、
//	“字符串区长度超过缓冲区MAX_BYTE限制”、
//	“数值区字符串转数据超过INT64限制”等错误
//  置状态位，
//或者清除错误标志（ErrFlag=ERROR_NONE）
void	CEncodeDecode::SetError(unsigned nErrFlag)
{
	if(nErrFlag)
		m_nErr |= nErrFlag;
	else
		m_nErr = nErrFlag;
}

void	CEncodeDecode::EditChanged(const CString &csNewString, int nCallerId)
{
	static struct DecodeType{int CallerId;CString* pTextString; void (CEncodeDecode::*DecodeFunc)();} decodeArray[]={	//每个编辑框对应一个字符串成员、一个解码函数
		{CallbyUnicode,		&m_csUnicode,		UnicodeEditToM		},
		{CallbyUTF8,		&m_csUtf8,			Utf8EditToM			},
		{CallbyAscii,		&m_csAscii,			AsciiEditToM		},
		{CallbyBase64,		&m_csBase64,		Base64EditToM		},
		{CallbyHex,			&m_csHex,			HexEditToM			},
		{CallbyHex2,		&m_csHex2,			Hex2EditToM			},
		{Callby64,			&m_cs_64,			R64EditToInt64		},
		{Callby32,			&m_cs_32,			R32EditToInt64		},
		{Callby16,			&m_cs_16,			R16EditToInt64		},
		{Callby10,			&m_cs_10,			R10EditToInt64		},
		{Callby8,			&m_cs_8,			R8EditToInt64		},
		{Callby2,			&m_cs_2,			R2EditToInt64		},
		{CallbyRadixValue,	&m_csRadixValue,	RValueEditToInt64	}
	};
		
	SetError(ERROR_NONE);
	for(int i=0; i<sizeof(decodeArray)/sizeof(struct DecodeType); i++)
	{
		struct DecodeType &aType = decodeArray[i];
		if(aType.CallerId == nCallerId)
		{
			*(aType.pTextString) = csNewString;
			(this->*(aType.DecodeFunc))();
			if((aType.CallerId & CallbyStringGroup) == CallbyStringGroup)
				MtoInt();	//如是修改字符串区，则先将数值数据流复制到字符串数据流
			else	//if((aType.CallerId & CallbyIntGroup) == CallbyIntGroup)	MtoInt();	
				InttoM();	//如是修改数值区，则先将字符串数据流复制到数值数据流

			Convert();		//根据内存或者数值转到其它各种格式
			return;
		}
	}
	ASSERT(0);	//不应该执行到此。漏定义 decodeArray 的话则触发
}

//把内存数据流或者数值依次按相应的编码规则转换成对应的显示字符串
void	CEncodeDecode::Convert()
{
//字符串区：
	MToUnicodeEdit();
	MToUtf8Edit();
	MToAsciiEdit();
	MtoBase64Edit();
	MtoHexEdit();
	MtoHex2Edit();
//数值区：
	Int64ToR64Edit();
	Int64ToR32Edit();
	Int64ToR16Edit();
	Int64ToR10Edit();
	Int64ToR8Edit();
	Int64ToR2Edit();
	Int64ToRadixValueEdit();
//哈希区：
	MtoMD5();
	MtoMD5B64();
	MtoSHA1();
}

//把Int数值区内存数据流转作字符串区内存数据流。因Int64，所以最多得到8字节。
//主要数据依据：m_nInt64Len。因此检查在调用前是否准确设置 m_nInt64Len，主要在A2I和Clear中
void	CEncodeDecode::InttoM()
{
	TryNewM(m_nInt64Len);
	memcpy(m_pBytes, &m_nInt64, m_nBytesLen);
	m_pBytes[m_nBytesLen]=0;
}

//把字符串区内存数据流转为Int64数值区内存数据流。因Int64，所以最多取前8字节。
void	CEncodeDecode::MtoInt()
{
	m_nInt64Len = m_nBytesLen;
	if(m_nInt64Len > 8)
	{
		m_nInt64Len = 8;
		SetError(ERROR_CUTSTRINGTO8);
	}
	m_nInt64=0;	//重置到全0
	memcpy(&m_nInt64, m_pBytes, m_nInt64Len);
	if(m_bSign && m_nInt64>0 && m_nInt64 & ((UINT64)1 << (m_nInt64Len*8-1) ))
		m_nInt64 -= (UINT64)1 << (m_nInt64Len*8);	//无符号时的最高位为1的正数，须变成负数
}

//计算出输入的m_csUnicode字符串视为Unicode方式时对应的字节数据流，存入到m_pBytes中。
void	CEncodeDecode::UnicodeEditToM()
{
	int nUnicodeLen;
	LPWSTR newUnicode = GetUnicodeFromEdit(m_csUnicode, nUnicodeLen);
	TryFreeM();
	m_pBytes = (char*)newUnicode;
	m_nBytesLen = sizeof(WCHAR) * nUnicodeLen;
}

//输入框字符串转Unicode，用于处理 Unicode 和 Utf8 输入框
//返回新 new 的 Unicode 缓冲区（并需要调用者释放）及其长度
LPWSTR	CEncodeDecode::GetUnicodeFromEdit(const CString& csString, int& nUnicodeLen)
{
#ifdef _UNICODE	// Unicode 模式下，“中1”的内存是0x4E2D 0x0031，其中0x4E2D由国际标准组织分配，0x0031在ASCII分配的0x31基础上扩展为双字节
	nUnicodeLen = csString.GetLength();
	LPWSTR newUnicode=new WCHAR[nUnicodeLen];
	memcpy(newUnicode, csString, sizeof(WCHAR)*nUnicodeLen);	//直接复制即可

#else			//	MBCS方式下，“中1”的内存是0xD0D6 0x31，0xD0D6由中国人分配
	nUnicodeLen = MultiByteToWideChar(CP_ACP, 0, csString, csString.GetLength(), NULL, 0);
	LPWSTR newUnicode=new WCHAR[nUnicodeLen];
	MultiByteToWideChar(CP_ACP, 0, csString, -1, newUnicode, nUnicodeLen);	// 0xD0D6 0x31 经 ASCII/MBCS 转为 Unicode 得到 0x4E2D 0x0031

#endif	//end of #ifdef _UNICODE...#else...

	return newUnicode;
}

//UnicodeEditToM()的逆：计算把m_pBytes视为Unicode方式时对应的显示字符串，存入到m_csUnicode中。
//如果 m_pBytes 长度不是 Unicode 字节数的整数被，则最后补0且视为高字节。
void	CEncodeDecode::MToUnicodeEdit()
{
	int nUnicodeLen = (m_nBytesLen + sizeof(WCHAR) -1)/sizeof(WCHAR);		// 朝上取整得到 nUnicodeLen，不含结束0占的1位。
									// 截止2010年，查到的都是MSVC只支持USC2，不知道是否支持 UCS4等？
	char *p = m_pBytes + m_nBytesLen;
	for (int i=(m_nBytesLen%sizeof(WCHAR)); i>0; i--)		// 补0对齐到Unicode字节数的整倍数，即不足Unicode字符的字节组的高字节填0
		*p++ = 0;

#ifdef _UNICODE	//Unicode 模式下，直接复制，即可显示
	//We desire a not protected m_csUnicode.AssignCopy(nUnicodeLen, (LPCTSTR)m_pBytes); like these 2:
	CString tmpStr((LPCWSTR)m_pBytes, nUnicodeLen);
	m_csUnicode = tmpStr;

#else			//MBCS方式下，进行转换后才能显示
	UnicodePtrToMBCSStr(m_csUnicode, (LPCWSTR)m_pBytes, nUnicodeLen);

#endif
	CheckCut0(m_csUnicode);
}

//对准备显示的字符串进行是否截0处理
//当有0被替换为. 时，返回 TRUE
BOOL	CEncodeDecode::CheckCut0(CString& csString)
{
	BOOL bCut = FALSE;
	if(!m_bCut0)
		for(int i=0; i<csString.GetLength(); i++)
		{
			if( csString[i] == 0 )
			{
				csString.SetAt(i, '.');	// csString[i] = 0 got "error C2106: '=' : left operand must be l-value"
				bCut = TRUE;
			}
		}
		return bCut;
}

//计算出输入的m_csUtf8字符串视为UTF8方式时对应的字节数据流，存入到m_pBytes中。
void	CEncodeDecode::Utf8EditToM()
{
	int nUnicodeLen;
	LPWSTR tempUnicode = GetUnicodeFromEdit(m_csUtf8, nUnicodeLen);
	int len = WideCharToMultiByte(CP_UTF8, 0, tempUnicode, nUnicodeLen, NULL, 0, NULL, NULL);
	if(TryNewM(len))
		WideCharToMultiByte(CP_UTF8, 0, tempUnicode, nUnicodeLen, m_pBytes, m_nBytesLen, NULL, NULL);
	m_pBytes[m_nBytesLen] = 0;
	delete tempUnicode;
}

//Utf8EditToM()的逆：计算把m_pBytes视为UTF8方式时对应的显示字符串，存入到m_csUtf8中。
void	CEncodeDecode::MToUtf8Edit()
{
	Utf8ToStr(m_csUtf8, m_pBytes, m_nBytesLen);
	CheckCut0(m_csUtf8);
}

//计算出输入的 m_csAscii 字符串视为 ASCII/GBK 方式时对应的字节数据流，存入到m_pBytes中。
void	CEncodeDecode::AsciiEditToM()
{
#ifdef _UNICODE	//Unicode 模式下，需进行 Unicode 到 MBCS 的转换
	int nUnicodeLen = m_csAscii.GetLength();
	m_nBytesLen = WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)m_csAscii, nUnicodeLen, NULL, 0, NULL, NULL);
	if(TryNewM(m_nBytesLen))
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)m_csAscii, nUnicodeLen, m_pBytes, m_nBytesLen, NULL, FALSE);
#else			//MBCS 模式下，直接复制
	m_nBytesLen = m_csAscii.GetLength();
	if(TryNewM(m_nBytesLen))
		memcpy(m_pBytes, m_csAscii, m_nBytesLen);
#endif

	m_pBytes[m_nBytesLen] = 0;
}

//AsciiEditToM 的逆：计算把m_pBytes视为 ASCII/GBK 方式时对应的显示字符串，存入到 m_csAscii 中。
void	CEncodeDecode::MToAsciiEdit()
{
#ifdef _UNICODE	//Unicode 模式下，需要转换后显示
	MBCSPtrToUnicodeStr(m_csAscii, m_pBytes, m_nBytesLen);
#else			//MBCS 模式，直接复制后即可显示
	SetCStringContent(m_csAscii, m_pBytes, m_nBytesLen);
#endif

	CheckCut0(m_csAscii);	//检查处理字符串中的0
}

void	CEncodeDecode::Base64EditToM()
{
	int len = m_csBase64.GetLength() * 3 / 4 + 2;
	TryNewM(len);

	unsigned err = base64Decode(m_pBytes, m_csBase64, m_nBytesLen);
	if (err) SetError(err);
}

void	CEncodeDecode::MtoBase64Edit()
{
	char *temp = new char[m_nBytesLen*4/3+4];
	base64Encode(temp, m_pBytes, m_nBytesLen);
	m_csBase64 = temp;
	delete temp;
}

//计算出MBCS方式下输入的m_csHex字符串视为Hex方式时对应的字节数据流，存入到m_pBytes中。
void	CEncodeDecode::HexEditToM()
{
	__int8	digitH,digitL;
	int	nLen=m_csHex.GetLength();	//Hex串的长度
	TryNewM((nLen + 1) / 2);			//按Hex串全部有效的情况分配缓冲区
	m_nBytesLen=0;
	for(int i = 0; i < nLen; i += 2)
	{
		if(i==nLen-1)	//Hex串长度为奇数时，最后一个符号表示低4位，高4位补0
		{
			digitH = 0;
			digitL = c2digit((char)m_csHex[i], 16);
		}
		else	//否则，前面是高4位，后面是低4位
		{
			digitH = c2digit((char)m_csHex[i], 16);
			digitL = c2digit((char)m_csHex[i+1], 16);
		}

		if (digitH == -1 || digitL == -1)	//输入的字符串中有非法字符，则置标志后结束转换。
		{
			SetError(ERROR_INVALIDCHAR);
			break;
		}
		else
			m_pBytes[m_nBytesLen++] = ((digitH<<4) | digitL);
	}
}

//内存数据流的16进制编码，顺序编码和UltraEdit或者WinHex等软件一样。
void	CEncodeDecode::MtoHexEdit()
{
	LPTSTR buf = m_csHex.GetBufferSetLength(m_nBytesLen*2);
	hexdump(buf, (unsigned char *)m_pBytes, m_nBytesLen);		// 注：此处如没有(unsigned char *)，则对0x80将得到FFFFFF80
	m_csHex.ReleaseBuffer();
}

//\x---- 格式转字符串。按大端方式转，另外，\x后不足4个字符则视为低位
void	CEncodeDecode::Hex2EditToM()
{
	TryNewM((m_csHex2.GetLength() + 5) * 2 / 6);	//按Hex2串全部有效的情况计算长度分配缓冲区 *2/6 : 6字节的"\xaabb" 会得到 2 字节的数据

	CString s;
	s = m_csHex2;
	s.MakeLower();
	m_nBytesLen = 0;
	CString sHex2Split;
	if (m_bXUType == XU_X )
		sHex2Split = _T("\\x");
	else
		sHex2Split = _T("\\u");
	
	int next=0;
	int next2=s.Find(sHex2Split, next);
	if ( ( next2==-1 || next2>0 ) && s.GetLength()>0)	//字符串中有内容但没有\x或者首个\x位置不靠前，则标错且不进行更多尝试
		SetError(ERROR_FORMAT);
	else
		while (next2==next)	//期望位置没有 \x ，则结束处理
		{
			next = next2+2;							// next指向本次 \x 后
			next2 = s.Find(sHex2Split, next);				// next2指向下一次 \x
			if ( next2 == -1 ) next2=s.GetLength();				// 或者字符串末尾
			if ( next2 > next+4 )				// 本组数据超出4个字符长度范围，则设置格式非法，并收回next2，致使本轮后即结束循环
			{
				next2 = next + 4;
				SetError(ERROR_FORMAT);
			}
			CString s4 = s.Mid(next, next2-next);
			if(s4.GetLength()<4)		//\x后不足4个字符则冠0处理为低位
			{
				s4=_T("0000") + s4;
				s4 = s4.Right(4);
			}
			TCHAR a[5];
			_tcscpy (a, s4);
			unsigned result;
			UINT64 i64	= AtoI(a, 16, &result);
			if (result == ERROR_INVALIDCHAR)
			{
				SetError(ERROR_INVALIDCHAR);
				break;
			}
			else	//\x后的一组有效数据
			{
				m_pBytes[m_nBytesLen++]= (unsigned char)(i64 & 0xFF );
				m_pBytes[m_nBytesLen++]= (unsigned char)((i64 & 0xFF00 ) >> 8);
			}

			next = next2;
			next2 = s.Find(sHex2Split, next);
		}
}

// 字符串每2字节一组转为 \x---- 或 \u---- 格式。如果 m_pBytes 中是奇数字节，则最后一组\x中的高字节补00
void	CEncodeDecode::MtoHex2Edit()
{
	m_csHex2 = _T( "" );
	TCHAR HEX4[5];
	unsigned short iHEX4;
	TCHAR c_XU;
	if (m_bXUType == XU_X)
		c_XU='x';
	else
		c_XU='u';

	for (int i=0; i<m_nBytesLen; i++)
	{
		m_csHex2 += '\\';
		m_csHex2 += c_XU;
		if (i<m_nBytesLen-1 || m_nBytesLen%2==0)
			iHEX4 = (unsigned char)m_pBytes[i+1];		// 注：此处如为 unsigned short，则对0x80将变成0xFF80
		else
			iHEX4 = 0;	//如果M长度为奇数，则最后一组\x中的高字节补00
		iHEX4 = (iHEX4 << 8) | ((unsigned char)m_pBytes[i]);
		ItoA(iHEX4, HEX4, 16, 4);
		m_csHex2 += HEX4;
		i++;
	}
}

//得到m_cs_64字符串按64进制编码对应的整数，存入m_nInt64
void	CEncodeDecode::R64EditToInt64()
{
	A2I(m_cs_64,64);
}

//R64EditToInt64()的逆：对m_nInt64作64进制编码，存入到m_cs_64中
void	CEncodeDecode::Int64ToR64Edit()
{
	I2A(m_cs_64,64);
}

void	CEncodeDecode::R32EditToInt64()
{
	A2I(m_cs_32,32);
}

void	CEncodeDecode::Int64ToR32Edit()
{
	I2A(m_cs_32,32);
}

void	CEncodeDecode::R16EditToInt64()
{
	A2I(m_cs_16,16);
}

void	CEncodeDecode::Int64ToR16Edit()
{
	I2A(m_cs_16,16);
}

void	CEncodeDecode::R10EditToInt64()
{
	A2I(m_cs_10,10);	//	和调用MS函数实现的效果一样：m_nInt64=_atoi64(m_cs_10);
}

void	CEncodeDecode::Int64ToR10Edit()
{
	I2A(m_cs_10,10);
}

void	CEncodeDecode::R8EditToInt64()
{
	A2I(m_cs_8,8);
}

void	CEncodeDecode::Int64ToR8Edit()
{
	I2A(m_cs_8, 8);
}

void	CEncodeDecode::R2EditToInt64()
{
	A2I(m_cs_2,2);
}

void	CEncodeDecode::Int64ToR2Edit()
{
	I2A(m_cs_2,2);
}

//按编辑框或spin按钮修改进制，可能因超范围被修改到默认范围
//返回实际设置的进制
UINT8	CEncodeDecode::SetRadix(UINT8 nNewRadix)
{
	m_nRadix = nNewRadix;
	if(m_nRadix < 2)
		m_nRadix = 64;
	else if(m_nRadix > 64)
		m_nRadix = 2;
	ItoA(m_nRadix, m_csRadix.GetBuffer(3), 10, 2);
	m_csRadix.ReleaseBuffer();
	return m_nRadix;
}

//spin事件处理方法调用本方法，更新进制基数并重算进制编码结果。
//不需要强壮性，不供更多调用主。
void	CEncodeDecode::RecalculateRadix()
{
	m_nRadix = (UINT8)AtoI(m_csRadix);;
	Int64ToRadixValueEdit();
}

//把输入的进制字符串按当前进制基数转换成数值
void	CEncodeDecode::RValueEditToInt64()
{
	A2I(m_csRadixValue,m_nRadix);
}

//RValueEditToInt64()的逆，把当前数值按当前进制基数转换成字符串
void	CEncodeDecode::Int64ToRadixValueEdit()
{
	I2A(m_csRadixValue,m_nRadix);
}

// xxxEditToInt64()的公共模块。把数值区输入框中的 srcText 字符串按进制要求转换成 Int64，保存到 m_nInt64 中；并得到数值的字节数，保存到 m_nInt64Len 中
// m_text_xxx对话框控件的格式可能没有足够的合法性限制，比如16进制框中可能输入'G'，因此需进行字符合法性校验
void	CEncodeDecode::A2I(const CString &csString, UINT8 nRadix)
{
	unsigned convErr;
	//当符号按钮关闭时输入了负数，则视为非法字符情况
	if(!m_bSign && csString.GetLength()>0 && csString[0]=='-')
	{
		convErr = ERROR_INVALIDCHAR;
		m_nInt64 = 0;	//且转换结果置为0
	}
	else
		m_nInt64 = AtoI(csString, nRadix, &convErr);

	if ( convErr == ERROR_NONE )
		CheckIntMemLen();

	if ( convErr != ERROR_NONE)
		SetError(convErr);
}

//对解码出来的 m_nInt64 根据符号开关情况，计算所需的最小内存长度
void	CEncodeDecode::CheckIntMemLen()
{
	INT64 i64tmp = m_nInt64;
	if(m_bSign && i64tmp<0)	///带符号情况下的负数取反，以便下面判断长度
		i64tmp = -i64tmp;	//关于边界问题的注释：-9223372036854775808(-0x8000000000000000) 的 - 结果仍为-0x8000000000000000
	//以及 -0x8000000000000000 减1 = 0x7FFFFFFFFFFFFFFF, 0x7FFFFFFFFFFFFFFF 加1 = -0x8000000000000000
	if		( i64tmp & 0xFFFFFFFF00000000 ) m_nInt64Len=8;
	else if	( i64tmp & 0x00000000FFFF0000 ) m_nInt64Len=4;
	else if	( i64tmp & 0x000000000000FF00 ) m_nInt64Len=2;
	else if	( i64tmp & 0x00000000000000FF ) m_nInt64Len=1;
	else m_nInt64Len=0;
	if(m_bSign && m_nInt64Len>0)	//带符号情况下非0数值，需要判断字节容纳能力
	{
		INT64 highBit1 = (INT64)1 << (m_nInt64Len*8-1);	//0x80 or 0x8000 or 0x80000000 or 0x8000000000000000
		if((m_nInt64>0 &&(i64tmp & highBit1))			//正数最高位为1
			|| (m_nInt64<0 && ((i64tmp-1) & highBit1)))	//或者负数小过 -highBit1，比如 -0x81 需要 2 Byte 来容纳
			m_nInt64Len *= 2;					//则需要下一个2倍长度的容量才能装下这个数
	}
}

//Int64ToxxxEdit()的公共模块， A2I的逆：对m_nInt64按radix进制编码，存入到 destText 中，按照通常的数值书写习惯，采用类大端（Big Endian）规则，也即高位在前。
void	CEncodeDecode::I2A(CString &csString, UINT8 nRadix)
{
	ItoA(m_nInt64, csString.GetBuffer(65), nRadix, 0, m_bSign);
	csString.ReleaseBuffer();
}

//按动“±符号”开关后的编解码处理
//返回新的开关状态
bool	CEncodeDecode::SwitchSign()
{
	m_bSign = !m_bSign;		//切换状态
	MtoInt();
	Convert();
	return m_bSign;
}

//按动“清空 Clear”按钮后的编解码处理
void	CEncodeDecode::ButtonClear()
{
	m_nInt64 = 0;
	m_nInt64Len = 0;			//作为清零动作的起点
	InttoM();
	Convert();
	SetError(ERROR_NONE);		//清空错误标志，确保
}

//翻滚十进制框右侧的spin时的编解码处理
void	CEncodeDecode::DeltaposSpin10(int nDelta)
{
	m_nInt64 += nDelta;	//不管有无符号，此处的加减1都是一样的结果
	CheckIntMemLen();
	InttoM();
	Convert();
}

void	CEncodeDecode::MtoMD5()
{
	CMD5 MyMD5;
	MyMD5.MDString((unsigned char *)m_pBytes, m_nBytesLen);
	m_csMd5 = _T( "" );
	for (__int8 i = 0; i<16; i++)
	{
		TCHAR digest_hex[3];
		ItoA(MyMD5.digest[i], digest_hex, 16, 2);
		m_csMd5 += digest_hex;
	}
}

void	CEncodeDecode::MtoMD5B64()
{
	CMD5 MyMD5;
	MyMD5.MDString((unsigned char *)m_pBytes, m_nBytesLen);
	char dest[25];
	base64Encode(dest, (char *)MyMD5.digest, 16);
	m_csMd5Base64=dest;
}

void	CEncodeDecode::MtoSHA1()
{
	CSHA1 MySHA1;
	unsigned int digest_array[5];
	TCHAR digest_hex[9];
	MySHA1.Input(m_pBytes, m_nBytesLen);
	MySHA1.Result(digest_array);
	//hexdump(dest,(char *)digest_array,20);	//尝试hexdump后，发现SHA1通常的输出格式为5个32bit按HEX数值输出，因此用下面的方式显示
	m_csSha1 = _T( "" );
	for (int i=0; i<5; i++)
	{
		ItoA(digest_array[i], digest_hex, 16, 8);	// 等同于sprintf(digest_hex,"%08X",digest_array[i]);
		m_csSha1 += digest_hex;
	}
}
