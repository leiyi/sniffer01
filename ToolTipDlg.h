#if !defined(AFX_TOOLTIPDLG_H__2843F028_F945_4210_B9AB_DC2DCEC360FF__INCLUDED_)
#define AFX_TOOLTIPDLG_H__2843F028_F945_4210_B9AB_DC2DCEC360FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ToolTipDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CToolTipDlg dialog

class CToolTipDlg : public CDialog
{
// Construction
public:
	CToolTipDlg(UINT uID, CWnd* pParent = NULL);  

// Dialog Data
	//{{AFX_DATA(CToolTipDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CToolTipDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CToolTipCtrl m_wndToolTip;

	// Generated message map functions
	//{{AFX_MSG(CToolTipDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CFont	m_Font;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOOLTIPDLG_H__2843F028_F945_4210_B9AB_DC2DCEC360FF__INCLUDED_)
