// ToolTipDlg.cpp : implementation file
//

#include "stdafx.h"
#include "01Sniffer.h"
#include "ToolTipDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _MBCS
#pragma message ("============================== 提 醒 ======================================================")
#pragma message ("* 本工程在 MBCS 模式下编译有缺陷：不能处理远东拉美等字符，因此没有实际使用意义            *")
#pragma message ("* 设置 MBCS 模式的目的是用于了解字符变量的内存占用在 MBCS 模式下和 Unicode 模式下的区别   *")
#pragma message ("* MBCS 模式的缺陷判断对不同语言的Windows其结果不一样，您知道结果和原因吗？                *")
#pragma message ("===========================================================================================")
#endif

/////////////////////////////////////////////////////////////////////////////
// CToolTipDlg dialog


CToolTipDlg::CToolTipDlg(UINT uID, CWnd* pParent /*=NULL*/)
	: CDialog(uID, pParent)
{
	//{{AFX_DATA_INIT(CToolTipDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void	CToolTipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CToolTipDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CToolTipDlg, CDialog)
	//{{AFX_MSG_MAP(CToolTipDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CToolTipDlg message handlers

BOOL	CToolTipDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// 创建提示信息控件 
	m_wndToolTip.Create(this);
	m_wndToolTip.SetMaxTipWidth(600);	//SetMaxTipWidth 之后，可以支持显示换行符，算个小窍门呵呵
	//下述数据拟存入 ini 文件
	m_wndToolTip.SetDelayTime(20*1000*60);	//显示20分钟//	m_wndToolTip.SetDelayTime(TTDT_AUTOPOP, 20*1000*60); 以前这么写，只有二三十秒钟
	m_wndToolTip.SetDelayTime(TTDT_INITIAL, 200);		//驻0.2秒即提示
//	m_wndToolTip.SetDelayTime(TTDT_RESHOW, 100);		//后续提示窗只需0.1秒即显示
	m_wndToolTip.SetTipBkColor(RGB(0x10,0x10,0x10));	//背景色：黑
	m_wndToolTip.SetTipTextColor(RGB(0x20,0xff,0x20));	//文字色：绿，穿越 APPLE BASIC 时代..........
	m_Font.CreatePointFont(110, _T("宋体"));
	m_wndToolTip.SetFont(&m_Font);

	RECT rect;
	rect.left=15; rect.top=8; rect.right=15; rect.bottom=8;
	m_wndToolTip.SetMargin(&rect);

	m_wndToolTip.Activate(TRUE);
	// 获得对话框的第一个控件(子窗口)指针
	CWnd *pWndChild = GetWindow(GW_CHILD);
	CString strToolTip;
	// 循环获得所有控件指针，并将提示信息加入到提示信息控件
	while (pWndChild)
	{
		int nID = pWndChild->GetDlgCtrlID();
		// 获得控件ID对应提示信息字符串
		if (strToolTip.LoadString(nID))
		{
			// 将控件对应的提示信息注册到提示信息控件里
			m_wndToolTip.AddTool(pWndChild, strToolTip);
		}
		// 获得对话框的下一个控件(子窗口)指针
		pWndChild = pWndChild->GetWindow(GW_HWNDNEXT);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL	CToolTipDlg::PreTranslateMessage(MSG* pMsg) 
{
	// WM_MOUSEFIRST : 第一个鼠标消息值，WM_MOUSELAST : 最后一个鼠标消息值
	// 因此下面一句代码就是判断所有的鼠标消息
	if (pMsg->message >= WM_MOUSEFIRST && pMsg->message <= WM_MOUSELAST)
	{
		MSG msg;
		::CopyMemory(&msg, pMsg, sizeof(MSG));
		// 获得消息结构里窗口句柄的父窗口句柄
		HWND hWndParent = ::GetParent(msg.hwnd);
		// 判断父窗口句柄是否是对话框句柄，若不是一直往上找，
		// 保证消息结构里窗口句柄是对话框的控件句柄
		while (hWndParent && hWndParent != m_hWnd)
		{
			msg.hwnd = hWndParent;
			hWndParent = ::GetParent(hWndParent);
		}
		// 如是对话框控件的消息，将鼠标消息发送给CToolTipCtrl控件，
		// 然后由控件来处理这些消息，判断是否显示提示信息
		if (msg.hwnd)
		{
			m_wndToolTip.RelayEvent(&msg);
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
