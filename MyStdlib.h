// MyStdlib.h 数进制转换等公共函数
//////////////////////////////////////////////////////////////////////

#if !defined(MYSTDLIB_INCLUDED_)
#define MYSTDLIB_INCLUDED_

#ifndef UINT64
	typedef unsigned _int64 UINT64;
#endif

#ifndef INT64
	typedef _int64 INT64;
#endif

#ifndef UINT8
	typedef unsigned __int8 UINT8;
#endif

#ifndef INT8
	typedef __int8 INT8;
#endif

#ifndef BYTE
	typedef unsigned char	BYTE;
#endif
	
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

char		getlowbyte(wchar_t wchar);
char		gethighbyte(wchar_t wchar);
char		digit2c(UINT8 digit, UINT8 radix);
__int8		c2digit(char c, UINT8 radix);

//C stdlib itoa() 的替代与增强: 进制支持到64，并支持填充头'0'或'A'到 padLen 长度，以达到 sprintf "%08X"的效果
//如果 padLen 小于itoa()结果的长度, 则不会填充'0'或'A'，效果等于普通的 itoa()
//当 i 的bit63为1且需要作为正数对待的时候，需把 isSigned 参数置 FALSE。
//返回转换出的字符串
TCHAR *		ItoA(INT64 i, TCHAR *a, UINT8 radix=10, unsigned padLen=0, bool isSigned=TRUE);

//C stdlib atoi() 的替代与增强
//注：如果 a 不由 "-" 号开头，且转出结果的最高位为1（即介于0x8000000000000000 ~0xffffffffffffffff 之间），则返回结果为负数，但根据C编译器的处理机制，仍可以无损地赋值给 UINT64 型变量得到正确结果（另外，此时 Err 并不会被置为 ERROR_OVERFLOW）
//返回：转换出的数值。注意即使遇到超长或者非法字符，也将返回最接近结果的值
//Err如果指向非空，则被置为 ERROR_NONE \ ERROR_INVALIDCHAR \ ERROR_OVERFLOW  之一。
//特别情况：如果 a 指向 NULL 或者长度为0，则返回0，且 Err=ERROR_INVALIDCHAR
INT64		AtoI(const TCHAR *a, UINT8 radix=10, unsigned* Err=NULL);
void		hexdump(TCHAR *dest, const unsigned char *src, unsigned slen);

//////////////////////////////////////////////////////////////////////
// Base64编解码函数，稍作改动可移植。
//	依赖于另外两个函数：digit2c 和 c2digit
#define BASE64_DEC_ERR_NONE		0
#define BASE64_DEC_ERR_FORMAT		0x1	//格式错误，比如\x错误。
#define BASE64_DEC_ERR_INVALIDCHAR	0x2	//非法字符

void		base64Encode(char *dest, const char *src, int slen);
unsigned	base64Decode(char *dest, const CString &src, int &dlen);
//////////////////////////////////////////////////////////////////////


// 字符串解码ERROR标志 ，其中0 1 2和上述Base64的一致。
#define ERROR_NONE		0	//清除
#define ERROR_FORMAT		0x1	//格式错误，比如\x错误。
#define ERROR_INVALIDCHAR	0x2	//非法字符
#define ERROR_OVERFLOW		0x4	//字符串区长度超过缓冲区MAX_BYTE限制，或者数值区字符串转数据超过INT64限制
#define ERROR_CUTSTRINGTO8	0x8	//字符串数据超过8字节，只截取前8字节作为INT


#endif // !defined(MYSTDLIB_INCLUDED_)

