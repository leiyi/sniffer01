//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by 01Sniffer.rc
//
#define ID_BUTTON_CLEAR                 3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAIN_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_PROMPT               1000
#define IDC_STATIC_STRING               1001
#define IDC_STATIC_DATA                 1002
#define IDC_STATIC_HASH                 1003
#define IDC_EDIT_BYTENUMSTR             1004
#define IDC_STATIC_ERRSTR               1005
#define IDC_EDIT_BYTENUMINT             1006
#define IDC_STATIC_ERRINT               1007
#define IDC_STATIC_UTF8                 1008
#define IDC_EDIT_UTF8                   1009
#define IDC_STATIC_UNICODE              1010
#define IDC_COMBO_UNICODE               1011
#define IDC_STATIC_ASCII                1012
#define IDC_EDIT_ASCII                  1013
#define IDC_CHECK_CUT0                  1014
#define IDC_STATIC_BASE64               1015
#define IDC_EDIT_BASE64                 1016
#define IDC_STATIC_HEX                  1017
#define IDC_EDIT_HEX                    1018
#define IDC_STATIC_HEX2                 1019
#define IDC_EDIT_HEX2                   1020
#define IDC_CHECK_XU                    1021
#define IDC_STATIC_64                   1022
#define IDC_EDIT_64                     1023
#define IDC_STATIC_32                   1024
#define IDC_EDIT_32                     1025
#define IDC_STATIC_16                   1026
#define IDC_EDIT_16                     1027
#define IDC_STATIC_10                   1028
#define IDC_EDIT_10                     1029
#define IDC_STATIC_8                    1030
#define IDC_EDIT_8                      1031
#define IDC_STATIC_2                    1032
#define IDC_EDIT_2                      1033
#define IDC_STATIC_RADIX                1034
#define IDC_EDIT_RADIX                  1035
#define IDC_SPIN_RADIX                  1036
#define IDC_STATIC_RADIXVALUE           1037
#define IDC_EDIT_RADIXVALUE             1038
#define IDC_STATIC_MD5                  1039
#define IDC_EDIT_MD5                    1040
#define IDC_STATIC_MD5B64               1041
#define IDC_EDIT_MD5B64                 1042
#define IDC_STATIC_SHA1                 1043
#define IDC_EDIT_SHA1                   1044
#define IDC_CHECK_SIGN                  1045
#define IDC_SPIN_10                     1046
#define IDS_HOMEPAGE                    1047
#define IDC_COMBO1                      1048
#define IDC_LIST1                       1049
#define IDC_STATIC_HOMEPAGE             1050

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
