// 01Sniffer.h : main header file for the 01Sniffer application
//

#if !defined(AFX_01SNIFFER_H__073EA5CE_DA8C_4228_B778_0BDDFEF57744__INCLUDED_)
#define AFX_01SNIFFER_H__073EA5CE_DA8C_4228_B778_0BDDFEF57744__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// C01SnifferApp:
// See 01Sniffer.cpp for the implementation of this class
//

class C01SnifferApp : public CWinApp
{
public:
	C01SnifferApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C01SnifferApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(C01SnifferApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_01SNIFFER_H__073EA5CE_DA8C_4228_B778_0BDDFEF57744__INCLUDED_)
