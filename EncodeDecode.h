// EncodeDecode.h
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_ENCDEC_H__A0D4A072_65DE_11D2_9816_9523BDBAF506__INCLUDED_)
#define AFX_ENCDEC_H__A0D4A072_65DE_11D2_9816_9523BDBAF506__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "MyStdlib.h"

//哪个编辑框的修改在触发调用批量转换功能
#define EDIT_NUM	13	//EDIT编辑框的个数

//按“清零”或者启动恢复上次数据时的调用参数，触发全部框修改
#define CallbyOther	0

//修改字符串区类型，0x11~0x1F
#define CallbyStringGroup		0x10	//只有下列 CallbyUnicode ~ CallbyHex2 与 CallbyStringGroup 作 & 与操作后等于 CallbyStringGroup
#define CallbyUnicode		0x11
#define CallbyUTF8			0x12
#define CallbyAscii			0x13
#define CallbyBase64		0x14
#define CallbyHex			0x15
#define CallbyHex2			0x16

//修改数值区类型，0x21~0x2F
#define CallbyIntGroup			0x20	//只有下列 Callby64 ~ CallbyRadixValue 与 CallbyIntGroup 作 & 与操作后等于 CallbyIntGroup
#define Callby64			0x21
#define Callby32			0x22
#define Callby16			0x23
#define Callby10			0x24
#define Callby8				0x25
#define Callby2				0x26
#define CallbyRadixValue	0x27

//////////////////////////////////////////////////////////////////////
// CEncodeDecode

class CEncodeDecode
{
public:
	CEncodeDecode();
//	virtual ~CEncodeDecode();
//	virtual int Decode( LPCTSTR szDecoding, LPTSTR szOutput ) = 0;
//	virtual CString Encode( LPCTSTR szEncoding, int nSize ) = 0;

	~CEncodeDecode();
//	int Decode( LPCTSTR szDecoding, LPTSTR szOutput );
//	CString Encode( LPCTSTR szEncoding, int nSize );
private:
	void	TryFreeM();				//释放字节流缓冲区
	bool	TryNewM(int nNewLen);	//分配 newLen 长度的字节流缓冲区
	void	InttoM();
	void	MtoInt();
	void	A2I(const CString &csString, UINT8 nRadix);	// 是否需要返回？ True:无64bit溢出; FALSE:64bit溢出
	void	I2A(CString &csString, UINT8 nRadix);
	void	CheckIntMemLen();
	BOOL	CheckCut0(CString& csString);	//对准备显示的字符串进行是否截0处理。 当有0被替换为. 时，返回 TRUE
	LPWSTR	GetUnicodeFromEdit(const CString& csString, int& nUnicodeLen);
//编码所对应的字节数据流及当前长度
	#define	XU_X	FALSE				// XU_X
	#define	XU_U	TRUE

	typedef struct Edit_UpdateFunc		//为处理所有输入框定义函数指针的控制矩阵结构
	{
		int	id;		//输入框的编号
		CEdit	*edit;		//输入框控件
		void	(CEncodeDecode::*pFunc)();	// 输入框中显示字符串到内存数据流的处理函数
		CString	m_text;		//接收字符串
	}  Edit_UpdateFunc;

public:
	void	SetError(unsigned nErrFlag);	//置内部错误
	void	Convert();
	void	RecalculateRadix();
	UINT8	SetRadix(UINT8 nNewRadix);
	void	EditChanged(const CString &csNewString, int nCallerID);
	bool	SwitchSign();
	void	ButtonClear();
	void	DeltaposSpin10(int nDelta);
	void	UnicodeEditToM();
	void	MToUnicodeEdit();
	void	Utf8EditToM();
	void	MToUtf8Edit();
	void	AsciiEditToM();
	void	MToAsciiEdit();
	void	Base64EditToM();
	void	MtoBase64Edit();
	void	HexEditToM();
	void	MtoHexEdit();
	void	Hex2EditToM();
	void	MtoHex2Edit();
	void	R64EditToInt64();
	void	Int64ToR64Edit();
	void	R32EditToInt64();
	void	Int64ToR32Edit();
	void	R16EditToInt64();
	void	Int64ToR16Edit();
	void	R10EditToInt64();
	void	Int64ToR10Edit();
	void	R8EditToInt64();
	void	Int64ToR8Edit();
	void	R2EditToInt64();
	void	Int64ToR2Edit();
	void	RValueEditToInt64();
	void	Int64ToRadixValueEdit();
	void	MtoMD5();
	void	MtoMD5B64();
	void	MtoSHA1();
	char*	m_pBytes;			//字符串缓冲区
	int	 	m_nBytesLen;		//字符串缓冲区有效长度。因缓冲区中可能含0所以不能用 strlen() 计算长度
	INT64	m_nInt64;			//Int64数值解码缓冲区
	int 	m_nInt64Len;		//数值解码缓冲区中有效数据的长度
	bool	m_bSign;			// “±符号”开关是否按下。将决定数值区当有符号方式处理还是无符号方式处理
	bool	m_bCut0;			// “截0”开关是否按下。将决定Ascii\Unicode\Utf8串中的0是当作结束符处理还是显示为 "."
	bool	m_bXUType;			// \x \u 的状态切换标志
	int		m_nErr;				//解码过程中是否出错记录，位操作模式
	UINT8	m_nRadix;			//进制基数。取值范围2~36，首次运行赋值36

	//定义将用作显示各种编码结果的字符串：
	CString m_csUnicode;
	CString m_csUtf8;
	CString m_csAscii;
	CString m_csBase64;
	CString m_csHex;
	CString m_csHex2;
	CString m_cs_64;
	CString m_cs_32;
	CString m_cs_16;
	CString m_cs_10;
	CString m_cs_8;
	CString m_cs_2;
	CString m_csRadix;
	CString m_csRadixValue;
	CString m_csMd5;
	CString m_csMd5Base64;
	CString m_csSha1;

};

#endif // !defined(AFX_ENCDEC_H__A0D4A072_65DE_11D2_9816_9523BDBAF506__INCLUDED_)

