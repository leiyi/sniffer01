; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "01sniffer.h"
LastPage=0

ClassCount=4
Class1=C01SnifferApp
Class2=C01SnifferDlg
Class3=CAboutDlg
Class4=CToolTipDlg

ResourceCount=2
Resource1=IDD_MAIN_DIALOG
Resource2=IDD_ABOUTBOX

[CLS:C01SnifferApp]
Type=0
BaseClass=CWinApp
HeaderFile=01Sniffer.h
ImplementationFile=01Sniffer.cpp
LastObject=C01SnifferApp

[CLS:C01SnifferDlg]
Type=0
BaseClass=CToolTipDlg
HeaderFile=01SnifferDlg.h
ImplementationFile=01SnifferDlg.cpp
LastObject=C01SnifferDlg
Filter=D
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=AboutDlg.h
ImplementationFile=AboutDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CToolTipDlg]
Type=0
BaseClass=CDialog
HeaderFile=ToolTipDlg.h
ImplementationFile=ToolTipDlg.cpp

[DLG:IDD_MAIN_DIALOG]
Type=1
Class=C01SnifferDlg
ControlCount=49
Control1=IDC_STATIC_PROMPT,static,1342308865
Control2=IDC_STATIC_STRING,button,1342177287
Control3=IDC_EDIT_BYTENUMSTR,edit,1350641793
Control4=IDC_STATIC_ERRSTR,static,1342308864
Control5=IDC_STATIC_UTF8,static,1342308864
Control6=IDC_EDIT_UTF8,edit,1350631552
Control7=IDC_STATIC_UNICODE,static,1342308866
Control8=IDC_COMBO_UNICODE,combobox,1344340226
Control9=IDC_STATIC_ASCII,static,1342308864
Control10=IDC_EDIT_ASCII,edit,1350631552
Control11=IDC_CHECK_CUT0,button,1342246915
Control12=IDC_STATIC_BASE64,static,1342308864
Control13=IDC_EDIT_BASE64,edit,1350631552
Control14=IDC_STATIC_HEX,static,1342308864
Control15=IDC_EDIT_HEX,edit,1350631560
Control16=IDC_STATIC_HEX2,static,1342308866
Control17=IDC_EDIT_HEX2,edit,1350631560
Control18=IDC_CHECK_XU,button,1342246915
Control19=IDC_STATIC_DATA,button,1342177287
Control20=IDC_EDIT_BYTENUMINT,edit,1350641793
Control21=IDC_STATIC_ERRINT,static,1342308864
Control22=IDC_CHECK_SIGN,button,1342246915
Control23=IDC_STATIC_10,static,1342308866
Control24=IDC_EDIT_10,edit,1350631552
Control25=IDC_SPIN_10,msctls_updown32,1342177458
Control26=IDC_STATIC_16,static,1342308866
Control27=IDC_EDIT_16,edit,1350631560
Control28=IDC_STATIC_64,static,1342308866
Control29=IDC_EDIT_64,edit,1350631552
Control30=IDC_STATIC_2,static,1342308866
Control31=IDC_EDIT_2,edit,1350631552
Control32=IDC_STATIC_8,static,1342308866
Control33=IDC_EDIT_8,edit,1350631552
Control34=IDC_STATIC_32,static,1342308866
Control35=IDC_EDIT_32,edit,1350631560
Control36=IDC_STATIC_RADIX,static,1342308866
Control37=IDC_EDIT_RADIX,edit,1350631552
Control38=IDC_SPIN_RADIX,msctls_updown32,1342177458
Control39=IDC_STATIC_RADIXVALUE,static,1342308866
Control40=IDC_EDIT_RADIXVALUE,edit,1350631552
Control41=IDC_STATIC_HASH,button,1342177287
Control42=IDC_STATIC_MD5,static,1342308866
Control43=IDC_EDIT_MD5,edit,1350641808
Control44=IDC_STATIC_MD5B64,static,1342308866
Control45=IDC_EDIT_MD5B64,edit,1350641808
Control46=IDC_STATIC_SHA1,static,1342308866
Control47=IDC_EDIT_SHA1,edit,1350641808
Control48=ID_BUTTON_CLEAR,button,1342242816
Control49=IDCANCEL,button,1342242816

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC_HOMEPAGE,static,1342308608

